/* eslint no-eval: 0 */
import React, { useState } from 'react'
import Functions from './components/Functions'
import MathOperations from './components/MathOperations'
import Numbers from './components/Numbers'
import Result from './components/Result'
import './App.css'

const App = () => {
  const [stack, setStack] = useState('')

  // const items = words(stack, /[^-^+^*+^/]+/g)

  return (
  <main className='react-calculator'>
    {/* <Result value={items[items.length-1]}/> Using words */}
    <Result value={stack}/>
    <Numbers
      onClickNumber={number => {
        setStack(`${stack}${number}`)
      }}
    />
    <div className="functions">
      <Functions
        onContentClear={() => {
          setStack('')
        }}
        onDelete={() => {
          if (stack.length > 0) {
            const newStack = stack.substring(0, stack.length - 1)
            setStack(newStack)
          }
        }}
      />
    </div>
    <section className="math-operations">
      <MathOperations
        onClickOperation={operation => {
          setStack(`${stack}${operation}`)
        }}
        onClickEqual={equal => {
          setStack(`${eval(stack).toString()}`)
        }}
      />
    </section>
  </main>)
}

export default App
